#include <iostream>
#include <iomanip>
#include "PrintMatrix.h"

using namespace std;

void PrintMatrix(int** Matrix, int matrixSize)
{
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{
			cout << setw(3) << Matrix[i][j];
		}
		cout << endl;
	}
}