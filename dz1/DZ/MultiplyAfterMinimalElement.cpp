#include <iostream>
#include "MultiplyAfterMinimalElement.h"
#include "PrintMatrix.h"

using namespace std;

void MultiplyAfterMinimalElement(int** Matrix, int matrixSize, int minimalNumber, int maximalNumber)
{
	system("cls");
	PrintMatrix(Matrix, matrixSize);

	cout << '\n';

	int multiplication = 1;
	int number = 0;
	int minValue = minimalNumber;

	for (int j = 0; j < matrixSize; j++)
	{
		multiplication = 1;
		number = 0;
		minValue = maximalNumber;

		for (int i = 0; i < matrixSize; i++)
		{
			if (abs(Matrix[i][j]) <= minValue)
			{
				minValue = abs(Matrix[i][j]);
				number = i;
			}
		}

		for (int i = 0; i < matrixSize; i++)
		{
			if (i > number)
			{
				multiplication *= Matrix[i][j];
			}
		}

		cout << j + 1 << "-� �������:" << endl;
		cout << '\t' << "����������� �� ������ �������: " << minValue << endl;
		cout << '\t' << "����� ������� �������� � �������: " << number + 1 << endl;
		cout << '\t' << "������������ ���������, ������� �����: " << multiplication << endl;
	}
}