#include <iostream>
#include "FindMaxAndSum.h"
#include "PrintMatrix.h"

using namespace std;

void FindMaxAndSum(int** Matrix, int matrixSize, int minimalNumber)
{
    system("cls");
    PrintMatrix(Matrix, matrixSize);

    cout << '\n';

    int border = (matrixSize * 2) / 3;
    int maxValue = minimalNumber;
    int sum = 0;

    for (int row = 0; row < matrixSize; row++)
    {
        for (int column = 0; column < border; column++)
        {
            sum += Matrix[row][column];

            if (Matrix[row][column] > maxValue)
            {
                maxValue = Matrix[row][column];
            }
        }
    }

    cout << "����� ��������� ������ " << border << " �������� �������:" << endl;
    cout << '\t' << "����� ���������: " << sum << endl;
    cout << '\t' << "������������ �������: " << maxValue << endl;
}