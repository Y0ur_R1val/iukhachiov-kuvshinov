#ifndef CREATE_MATRIX_H
#define CREATE_MATRIX_H

int** CreateMatrix(int matrixSize, int minimalNumber, int maximalNumber);

#endif