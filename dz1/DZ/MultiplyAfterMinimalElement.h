#ifndef MULTIPLY_AFTER_MINIMAL_ELEMENT_H
#define MULTIPLY_AFTER_MINIMAL_ELEMENT_H

void MultiplyAfterMinimalElement(int** Matrix, int matrixSize, int minimalNumber, int maximalNumber);

#endif