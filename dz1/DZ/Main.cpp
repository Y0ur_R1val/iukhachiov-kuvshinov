#include <iostream>
#include "CreateMatrix.h"
#include "PrintMatrix.h"
#include "MultiplyAfterMinimalElement.h"
#include "FindSameElementsInColumns.h"
#include "FindMaxAndSum.h"

using namespace std;

const int MIN_RANDOM_NUMBER = -6;
const int MAX_RANDOM_NUMBER = 6;

int MatrixSize{};

int main()
{
	setlocale(LC_ALL, "Russian");

	int** Matrix{};
	bool AppIsWorking = true;

	cout << "������� ����������� ���������� �������: ";
	cin >> MatrixSize;
	cout << endl;

	Matrix = CreateMatrix(MatrixSize, MIN_RANDOM_NUMBER, MAX_RANDOM_NUMBER);
	PrintMatrix(Matrix, MatrixSize);
	
	while (AppIsWorking)
	{
		int taskNumber = 0;

		cout << '\n' << "������� ����� ������������� ��� ������� (1-3), ��� 0, ���� ������� �����: ";
		cin >> taskNumber;

		if (taskNumber < 0 || taskNumber > 3)
		{
			system("cls");

			cout << "����������� �������: " << MatrixSize << '\n' << '\n';

			PrintMatrix(Matrix, MatrixSize);

			cout << '\n' << "������� ������������ ��������. ���������� �����." << endl;
		}
		else
		{
			switch (taskNumber)
			{
			case 0:
				cout << '\n' << "��������� ��������� ���� ������" << endl;
				AppIsWorking = false;
				break;
			case 1:
				MultiplyAfterMinimalElement(Matrix, MatrixSize, MIN_RANDOM_NUMBER, MAX_RANDOM_NUMBER);
				break;
			case 2:
				FindSameElementsInColumns(Matrix, MatrixSize);
				break;
			case 3:
				FindMaxAndSum(Matrix, MatrixSize, MIN_RANDOM_NUMBER);
				break;
			default:
				cout << "������ �� ���������" << endl;
				break;
			}
		}
	}

	return 0;
}