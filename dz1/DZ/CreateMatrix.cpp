#include <iostream>
#include <ctime>
#include "CreateMatrix.h"

int** CreateMatrix(int matrixSize, int minimalNumber, int maximalNumber)
{
	srand(time(0));

	int** Matrix = new int* [matrixSize];

	for (int i = 0; i < matrixSize; i++)
	{
		Matrix[i] = new int[matrixSize];
	}

	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{
			Matrix[i][j] = minimalNumber + (rand() % (maximalNumber - minimalNumber + 1));
		}
	}

	return Matrix;
}