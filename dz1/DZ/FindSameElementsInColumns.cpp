#include <iostream>
#include "FindSameElementsInColumns.h"
#include "PrintMatrix.h"

using namespace std;

void FindSameElementsInColumns(int** Matrix, int matrixSize)
{
	system("cls");
	PrintMatrix(Matrix, matrixSize);

	cout << '\n';

	int firstColumn = 0;
	int lastColumn = matrixSize - 1;
	int rowInFirstColumn = 0;
	int rowInLastColumn = 0;

	while (firstColumn < matrixSize)
	{
		if (Matrix[rowInFirstColumn][firstColumn] != Matrix[rowInLastColumn][lastColumn])
		{
			if (rowInLastColumn == matrixSize - 1)
			{
				rowInFirstColumn++;
				rowInLastColumn = 0;
			}
			else
			{
				rowInLastColumn++;
			}
		}
		else
		{
			cout << "������� ������� �������� � ������ � ��������� �������� �������" << endl;
			break;
		}
	}
}